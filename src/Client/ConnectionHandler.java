package Client;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Created by Johan on 2015-05-25.
 */
public class ConnectionHandler {

    private String IP;
    private int port;

    public ConnectionHandler () {

    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) { this.port = port; }

    public void showConnectBox () {

        Stage window = new Stage();

        window.setTitle("Connect to Server");
        window.getIcons().add(new Image("https://support.hubris.net/images/icons/128/network_connections.jpg"));

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));        //sets a padding on the window (in pixels)
        grid.setVgap(8);    //vertical spacing (in pixels)
        grid.setHgap(10);   //horizontal spacing (in pixels)
        grid.setStyle("-fx-background-color: #383838");

        //IP-address label
        Label IPlabel = new Label("IP-address");
        IPlabel.setStyle("-fx-text-fill: #e8e8e8");     //inline styling with css.
        GridPane.setConstraints(IPlabel, 0, 0);   //what item, what row, what column (starts at zero)

        //IP-address input
        TextField IPinput = new TextField();
        IPinput.setPromptText("Example: 192.168.0.1");
        GridPane.setConstraints(IPinput, 1, 0);

        //Port label
        Label portLabel = new Label("Port");
        portLabel.setStyle("-fx-text-fill: #e8e8e8");
        GridPane.setConstraints(portLabel, 0, 1);

        //port input
        TextField portInput = new TextField();
        portInput.setPromptText("Example: 1234");
        GridPane.setConstraints(portInput, 1, 1);

        //connect button
        Button connectButton = new Button("Connect");
        GridPane.setConstraints(connectButton, 1, 2);
        connectButton.setOnAction(e -> {
            IP = IPinput.getText();
            port = Integer.parseInt(portInput.getText());
            setIP(IP);
            setPort(port);
            window.close();
        });

        //add everything to grid.
        grid.getChildren().addAll(IPlabel, IPinput, portLabel, portInput, connectButton);
        Scene scene = new Scene (grid, 350, 200);
        window.setScene(scene);
        window.setResizable(false);
        window.showAndWait();
    }
}
