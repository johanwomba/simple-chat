package Client;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Client  {

    private String name;

    public Client() {

        name = "User";

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String setNameBox () {

        Stage window = new Stage();
        window.getIcons().add(new Image("http://icdn.pro/images/en/m/s/msn-icone-6172-128.png"));
        window.setTitle("Choose name");

        //Make a form
        TextField nameInput = new TextField();
        nameInput.setPromptText("Enter name here...");

        Button button = new Button("Done");
        button.setStyle("-fx-font-weight: bold; " +
                        "-fx-background-color: linear-gradient(#AB4642, #DC9656);" +
                        "-fx-text-fill:#FFFFFF; " +
                        "-fx-background-radius:4;");
        button.setOnAction(e -> {
            name = nameInput.getText();
            window.close();
        });

        //info label at the bottom.
        Label infoLabel = new Label("The user name you choose will\nonly be valid in the current session.");
        infoLabel.setStyle("-fx-text-fill: #AB4642");

        //layout
        VBox layout = new VBox(10);
        layout.setStyle("-fx-background-color: linear-gradient(#86C1B9, #E8E8E8)");
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(nameInput, button, infoLabel);

        Scene scene = new Scene(layout, 275, 165);
        window.setScene(scene);
        window.setResizable(false);
        window.show();

        return name;
    }

}
