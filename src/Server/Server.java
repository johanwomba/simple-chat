package Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Johan on 2015-05-19.
 */
public class Server extends JFrame {

    private JTextField userText;		//where you type
    private JTextArea chatWindow;		//chat window

    private ObjectOutputStream output;	//send messages
    private ObjectInputStream input;	//recieve messages

    private ServerSocket server;
    private Socket connection;

    //constructor

    public Server() {

        super("Server");		//name of server side
        userText = new JTextField();
        userText.setEditable(false);	//initial value false. If you dont have connection to anyone, you cant write.

        //when a user presses enter, sends text
        userText.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        sendMessage(event.getActionCommand());
                        userText.setText(""); 		//when message is sent, set text to nothing.
                    }
                }
        );
        add(userText, BorderLayout.SOUTH);
        chatWindow = new JTextArea();
        add(new JScrollPane(chatWindow));
        setSize(300, 150);
        setVisible(true);
    }

    //setup and run server
    public void startRunning() {

        try {
            server = new ServerSocket(6789, 30); //6789 = port number (dont go lower than 1024),
                                                 //30 = (backlog)number of users that can use server.
            while (true) {
                try{
                    //connect and start conversation
                    waitForConnection();
                    setupStreams();
                    messageLoop();
                } catch (EOFException eofE) {	//EOF = end of stream exception.
                    showMessage("Error: " + eofE.getMessage());
                } finally {
                    //close streams, sockets. All things necessary.
                    shutDown();
                }
            }
        }
        catch(IOException ioe) {
            showMessage(ioe.getMessage());
        }
    }

    //wait for connection, then display connection info.
    private void waitForConnection() throws IOException {

        showMessage("Waiting... \n");
        //waits for someone to connect, and finally ACCEPTS the connection, and actually creates the socket.
        connection = server.accept();
        showMessage("You're connected with " + connection.getInetAddress().getHostName());
        //connection.getInetAddress().getHostName()  = the clients IP address.
    }

    //get stream to send and recieve data
    private void setupStreams() throws IOException {

        //create pathway to recieve messages from others.
        input = new ObjectInputStream(connection.getInputStream());
        //create pathway to send messages to others.
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
    }

    //during the chat conversation
    private void messageLoop() throws IOException {

        String message = "You are now connected!";
        sendMessage(message);
        ableToType(true);

        do {
            try {
                message = (String) input.readObject();	//treats the user input object as a string
                showMessage("\n" + message);	        //message = whatever the user typed.
            } catch (ClassNotFoundException cnfe) {
                showMessage("Error: " + cnfe.getMessage());	//if user sends something else than a string.
            }

        } while(true);

    }

    //close the streams and sockets.
    private void shutDown() {

        showMessage("\n Closing connections... \n");
        ableToType(false);
        try {

            output.close();			//close the availability to send stuff.
            input.close();			//close the availability to get stuff.
            connection.close();		//close the socket and therefore port.

        } catch (IOException ioe) {
            showMessage("Error: " + ioe.getMessage());
        }
    }

    //send message to client
    private void sendMessage(String message) {

        try {
            output.writeObject("SERVER says:\t" + getTime() + "\n\t" + message);
            showMessage("\nSERVER - " + message);
            output.flush();

        } catch (IOException ioe) {
            showMessage("Error: " + ioe.getMessage());
        }
    }

    //actually makes text appear on screen.
    private void showMessage(final String text) {
        //updates the GUI with help from a thread
        //only when someone sends something new.
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        chatWindow.append(text);
                    }
                }
        );
    }

    //makes the text field editable so user can type stuff.
    private void ableToType(final boolean ableType) {

        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        userText.setEditable(ableType);
                    }
                }
        );
    }

    //Gets the current time, and formats it as 2015/01/01 12:00
    public String getTime () {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date date = new Date();

        String dateTime = "(" + dateFormat.format(date) + ")";

        return dateTime;
    }
}
