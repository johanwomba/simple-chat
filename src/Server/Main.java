package Server;

import javax.swing.*;

/**
 * Created by Johan on 2015-05-19.
 */
public class Main {

    public static void main(String[] args) {

        Server server = new Server();
        server.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        server.startRunning();
    }
}