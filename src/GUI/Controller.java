package GUI;

import Client.Client;
import Client.ConnectionHandler;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Controller {


    @FXML
    private TextField typingField;
    @FXML
    private TextArea chatField;
    @FXML
    private Button sendButton;
    @FXML
    private CheckMenuItem enterCheck;
    @FXML
    private MenuItem chooseName;

    private String message = "";

    private Client user = new Client();
    private ConnectionHandler ipAndPort = new ConnectionHandler();



    private ObjectOutputStream output;
    private ObjectInputStream input;
    private Socket connection;



    //Updates GUI with the message typed.
    @FXML
    private void showMessage(final String text) {
        Platform.runLater(() -> chatField.appendText("\n" + text));
    }
    //Gets the text from typingField, formats it a bit, and then sends it to sendMessage(message)
    @FXML
    private void sendMessage() {


        typingField.setOnAction(
                e -> {
                    if (!typingField.getText().equals("")) {
                        message = user.getName() + " says:\t" + getTime() + "\n\t" + typingField.getText();
                        sendMessage(message);   //send to output method.
                        typingField.clear();
                        typingField.setPromptText("Type message here...");  //when message is sent, set prompt text.
                    }
                });
    }
    //actually sends and shows message.
    private void sendMessage (String message) {

        try {
            output.writeObject(message);
            showMessage(message);
            output.flush();

        } catch (IOException ioe) {
            showMessage("Error: " + ioe.getMessage());
        }

    }
    //Opens nameBox so user can choose name. This method can only run once per session.
    @FXML
    private void chooseNameClicked () {
        user.setNameBox();
        chooseName.setDisable(true);
    }
    //deletes the typingField.
    @FXML
    private void deleteItemClicked () {
        typingField.clear();
    }

    //acts the same as sendMessage() but goes when user clicks "Send"
    @FXML
    private void sendButtonClicked() {

        sendButton.setOnAction(e -> {
            message = user.getName() + " says:\t" + getTime() + "\n\t" + typingField.getText(); //a bit of formatting..
            sendMessage(message);
            typingField.clear();
            typingField.setPromptText("Type message here...");
        });
    }
    //Unnecessary feature, just for fun.
    @FXML
    private void checkBoxChecked() {

        if (enterCheck.isSelected()) {
            sendButton.setDisable(false);
        } else {
            sendButton.setDisable(true);
        }
    }
    //another kind of fun and unnecessary feature.
    //
    @FXML
    private void openReadMe () {
        try {
            File readme = new File("C:/Systemutveckling 2014-2016/IntelliJ projects/ChattN/readMe.html");
            Desktop.getDesktop().open(readme);

        } catch (IOException e) {
            showMessage("Error: " + e.getMessage());
        }

    }
    //When user clicks "Connect...", it shows the connectBox, clears chatField and startRunning()
    @FXML
    private void connectBtnClicked () {

            ipAndPort.showConnectBox();
            startRunning();
            chatField.clear();
    }

    /**
     * Sets up the socket.
     * Takes the IP and port the user checked in during connectBox.
     * @throws IOException
     */
    public void connectToServer() {

        showMessage("Attempting connection..");
        try {
            connection = new Socket(InetAddress.getByName(ipAndPort.getIP()), ipAndPort.getPort());
            connection.setSoTimeout(500);
        } catch (IOException ioE) {
            showMessage("Error: Could not connect to server.");
        }
    }

    /**
     * Sets everything up and keeps messageLoop() alive.
     *
     */
    private void startRunning() {
        //Emergency solution. When user wants to quit, shutDown().
        typingField.getScene().getWindow().setOnCloseRequest(event -> {
            if (QuitBox.display()) {
                if (connection != null) {
                    shutDown();
                } else {

                }
            }
            else {
                event.consume();    //overrides quit-button and does nothing. I.E doesn't close program.
            }
        });
        try {
            connectToServer();
            typingField.setEditable(true);
            setupStreams();
            messageLoop();
        } catch (EOFException eof) {
            showMessage("Error: " + eof.getMessage());
        } catch (IOException ioE) {
            showMessage("Error: " + ioE.getMessage());
        }
    }

    /**
     * Initializes the streams, and flushes output (good practice to do so.)
     * @throws IOException
     */
    private void setupStreams() throws IOException {

        output = new ObjectOutputStream(connection.getOutputStream());
        input = new ObjectInputStream(connection.getInputStream());
        output.flush();
    }

    /**
     * Closes input- and outputstream as well as the socket.
     * Good cleanup so the server can accept another user.
     */
    public void shutDown() {

        showMessage("\n Closing connections... \n");
        typingField.setEditable(false);
        try {

            if (output != null)     output.close();			//close the availability to send stuff.
            if (input != null)      input.close();			//close the availability to get stuff.
            if (connection != null) connection.close();		//close the socket and therefore port.

        } catch (IOException ioE) {
            showMessage("Error: " + ioE.getMessage());
        }
    }

    /**
     * Always checks if something new arrives in inputstream.
     * If so, showMessage on textarea.
     */
    private void messageLoop() {

        Thread loop = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    try {
                        message = (String) input.readObject();    //treats the user input object as a string
                        showMessage(message);                    //message = whatever the user typed.
                    } catch (ClassNotFoundException cnfE) {
                        showMessage("\n\tUnexpected object class: " + cnfE.getMessage());    //if user sends something else than a string.
                    } catch (SocketTimeoutException ste) {
                        //every time readObject cant find text, it throws this ex.
                    } catch (IOException ie) {

                    }
                } while (true);
            }
        });
        loop.setDaemon(true);   //background thread, app can be exited without closing this thread.
        loop.start();
    }

    /**
     * Method captures the current date and time. Used in messages, and soon in logger.
     * But then with milliseconds as well.
     * @return - the actual time.
     */
    public String getTime () {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date date = new Date();

        String dateTime = "(" + dateFormat.format(date) + ")";

        return dateTime;
    }
}