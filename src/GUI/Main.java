package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application {

    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {

        window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));   //gets info from FXML-file
        window.getIcons().add(new Image("http://icdn.pro/images/en/m/s/msn-icone-6172-128.png"));   //sets icon
        window.setTitle("Instant messenger");   //sets title
        root.getStylesheets().add(getClass().getResource("myStyle.css").toExternalForm());  //gets style from CSS
        window.setScene(new Scene(root, 605, 310));
        window.show();
        window.setOnCloseRequest(e -> {
            e.consume();            //overrides closeRequest.
            closeProgram();
        });
    }

    public static void main(String[] args) { launch(args); }

    public void closeProgram() {
        boolean answer = QuitBox.display(); //opens quitbox, and returns boolean value
        if (answer) {
            window.close();
        }
    }
}

//if time -> file sharing