package GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Johan on 2015-05-21.
 */
public class QuitBox extends Main {

    static boolean answer;

    public static boolean display () {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);    //block other windows until this one is done.
        window.getIcons().add(new Image("http://icdn.pro/images/en/m/s/msn-icone-6172-128.png"));
        window.setTitle("Quit");
        window.setMinWidth(250);
        window.setMinHeight(150);
        Label label = new Label();
        label.setText("Are you sure?");

        //create two buttons
        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        yesButton.setOnAction(e -> {
            answer = true;
            window.close();
        });

        noButton.setOnAction(e -> {
            answer = false;
            window.close();
        });

        VBox layout = new VBox (10);
        layout.setStyle("-fx-background-color: linear-gradient(#86C1B9, #E8E8E8)");
        layout.getChildren().addAll(label, yesButton, noButton);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.setResizable(false);
        window.showAndWait();

        return answer;
    }

}
